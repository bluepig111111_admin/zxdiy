const app = getApp();
Component({
  properties: {
    detailInfo: Object
  },
  data: {
    color: app.ext.color
  },
  attached: function () {
    // console.log(this)
    console.log(this.data.detailInfo)
    if(this.data.detailInfo.skuStatus==true){

    }else{
      
    }
  },
  methods: {
    selectSpec(e) {
      const item = e.currentTarget.dataset.item;
      console.log(item)
      const list = e.currentTarget.dataset.list;
      console.log(list)
      let d = this.data.detailInfo
      d.skuName = ''
      this.setData({
        detailInfo : d
      })
      const detailInfo = this.data.detailInfo;
      if (detailInfo.skuData.length>0) {
        detailInfo.skuData.forEach(val => {
          if (val.ggmx == item.ggmx) {
            val.ggsx.forEach(sku => {
              if (sku.name == list.name) {
                sku.is_select = 1;
                if(detailInfo.skuName==''){
                  detailInfo.skuName = sku.name
                }else{
                  detailInfo.skuName = detailInfo.skuName + "-" + sku.name
                }
                
                detailInfo.skuStatus = true
                
              } else {
                sku.is_select = 0;
              }
            })
          }else{
            val.ggsx.forEach(sku => {
              if (sku.name != list.name) {
                if(sku.is_select==1){
                  if (detailInfo.skuName == '') {
                    detailInfo.skuName = sku.name
                  } else {
                    detailInfo.skuName = detailInfo.skuName + "-" + sku.name
                  }
                }
              } 
            })
          }
        })
        detailInfo.gg.con.forEach(val=> {
          console.log(detailInfo.skuName)
          console.log(val.name)
          if(detailInfo.skuName==val.name){
            detailInfo.skuPrice = val.price
          }
        })
      }
      this.setData({
        detailInfo: detailInfo
      })
      console.log(this.data.detailInfo)
    },
    bindinput(e) {
      let num = e.detail.value;
      if (num.length == 1) {
        num = num.replace(/[^1-9]/g, '0')
      } else {
        num = num.replace(/\D/g, '')
      }
      this.data.detailInfo.num = ~~num ? ~~num : 1;
      this.setData({
        detailInfo: this.data.detailInfo
      })
    },
    changeNum(e) {
      const type = Number(e.currentTarget.id);
      if (type) {
        this.data.detailInfo.num++;
      } else {
        if (this.data.detailInfo.num > 1) {
          this.data.detailInfo.num--;
        }
      }
      this.triggerEvent('changeNum', this.data.detailInfo);
      this.setData({
        detailInfo: this.data.detailInfo
      })
    },
    buyNowOrAddCart() {
      this.triggerEvent('buyNowOrAddCart', this.data.detailInfo);
    }
  }
});